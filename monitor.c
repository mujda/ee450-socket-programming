#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#define AWS_PORT "26393"
#define HOST "localhost"
#define MAXDATASIZE 100


//Beej's Guide to Network Programming for get_in_addr
void *get_in_addr(struct sockaddr *sa) {
	if(sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

//Adapted Beej's Guide to Network Programming to establish TCP connection with AWS
/*
* 1)Receive client input, parse string and display linkID, power and size
* 2)Display appropriate message depending on whether match is found
*   - If no matches found: Found no matches for link
*   - If match found: Display transmission delay, propagation delay and end-to-end delay in that order and with new lines in between
*/
int main(int argc, char* argv[]) {
	int sockfd;
	int numbytes;

	char buf[MAXDATASIZE];

	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	int rv;
	char s[INET6_ADDRSTRLEN];

	if(argc != 1) {
		fprintf(stderr, "usage: client hostname\n");
		exit(1);
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	printf("The monitor is up and running.\n");

	while(1) {

	if((rv = getaddrinfo(HOST, AWS_PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if(connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	freeaddrinfo(servinfo);

		char client_input[1024];

		if(recv(sockfd, client_input, 1024, 0) == -1) {
			perror("recv");
			exit(1);
		}


		char* linkID_str = strtok(client_input, ",");
		char* size_str = strtok(NULL, ",");
		char* power_str = strtok(NULL, ",");

		printf("The monitor received link ID=<%s>, size=<%s>, and power=<%s> from the AWS.\n", linkID_str, size_str, power_str);

		char result[1024];

		if(recv(sockfd, result, 1024, 0) == -1) {
			perror("recv");
			exit(1);
		}

		if(strcmp(result, "no matches") == 0) {
			printf("Found no matches for link <%s>.\n", linkID_str);
		} else {
			char* trans_delay = strtok(result, ",");
			char* prop_delay = strtok(NULL, ",");
			char* end_delay = strtok(NULL, ",");

			printf("The result for link <%s>:\nTt = <%s>ms,\nTp = <%s>ms,\nDelay = <%s>ms\n", linkID_str, trans_delay, prop_delay, end_delay);
		}
	}
}