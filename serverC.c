#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#define PORT_C "23393"
#define MAXBUFLEN 100

extern char* compute(char* client_input, char* link_info);

//Beej's Guide to Network Programming for get_in_addr
void *get_in_addr(struct sockaddr *sa) {
	if(sa->sa_family = AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

//Adapted Beej's Guide to Network Programming to establish UDP connection with AWS
/*
* 1) Establish UDP connection with AWS
* 2) Receive client_input (linkID, size, power)
* 3) Receive link_info (linkID, bandwidth, length, velocity, noise power)
* 4) Compute transmission delay, propagation delay and end-to-end delay (see compute.c for details)
* 5) Sends result (transmission delay, propagation delay and end-to-end delay) to AWS via UDP
*/

int main(void) {
	int sockfd;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	int rv;
	int numbytes;
	struct sockaddr_storage their_addr;
	char buf[MAXBUFLEN];
	socklen_t addr_len;
	char s[INET6_ADDRSTRLEN];

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	

	if((rv = getaddrinfo(NULL, PORT_C, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}
		if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	freeaddrinfo(servinfo);

	printf("The Server C is up and running using UDP on port <%s>.\n", PORT_C);

	while(1) {

		addr_len = sizeof(their_addr);
		if((numbytes = recvfrom(sockfd, buf, MAXBUFLEN-1, 0, (struct sockaddr*)&their_addr, &addr_len)) == -1) {
			perror("recvfrom");
			exit(1);
		}

		inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*)&their_addr), s, sizeof(s));
		buf[numbytes] = '\0';

		char* client_input = strdup(buf);
		char* client_input_cpy = strdup(client_input);

		if((numbytes = recvfrom(sockfd, buf, MAXBUFLEN-1, 0, (struct sockaddr*)&their_addr, &addr_len)) == -1) {
			perror("recvfrom");
			exit(1);
		}

		inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*)&their_addr), s, sizeof(s));
		buf[numbytes] = '\0';
		
		char* linkID_str = strtok(client_input,",");
		char* size_str = strtok(NULL, ",");
		char* power_str = strtok(NULL, ",");

		printf("The Server C received link information of link <%s>, file size <%s>, and signal power <%s>.\n", linkID_str, size_str, power_str);

		char*link_info = strdup(buf);

		//char* result = malloc(sizeof(char*));
		char* result = compute(client_input_cpy, link_info);

		printf("The Server C finished the calculation for link <%s>.\n", linkID_str);

		if(sendto(sockfd, result, sizeof(char)*(strlen(result)+1), 0, (struct sockaddr*)&their_addr, addr_len) == -1) {
				perror("sendto");
				exit(1);
		}

		printf("The Server C finished sending the output to AWS.\n");
	}

	return 0;

}