#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#define UDP_PORT "24393"
#define TCP_CLIENT "25393"
#define TCP_MONITOR "26393"

#define PORT_A "21393"
#define PORT_B "22393"
#define PORT_C "23393"

#define BACKLOG 10

//Beej's Guide to Network Programming for sigchld_handler
void sigchld_handler(int s) {
	int saved_errno = errno;

	while(waitpid(-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
}

//Beej's Guide to Network Programming for get_in_addr
void* get_in_addr(struct sockaddr *sa) {
	if(sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


//Adapted Beej's Guide to Network Programming to establish UDP connection with ServerC

/* void sendToComputingServer(char* client_input, char* link_info, char* result)
* 1) Establishes UDP connection with ServerC
* 2) Sends client input to serverC via UDP
* 3) Sends link info to serverC via UDP
* 4) Computes result (transmission delay, propagation delay and end-to-end delay) (see compute.c for details)
* 5) Recives result via UDP and stores in result
*/
void sendToComputingServer(char* client_input, char* link_info, char* result) {
	int sockfd;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	int rv;
	int numbytes;

	char server = 'C';
	char* server_port = PORT_C;


	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;

	if((rv = getaddrinfo("localhost", server_port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("talker:socket");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "talker: failed to create socket\n");
		exit(1);
	}

	if((numbytes = sendto(sockfd, client_input, (sizeof(char*) * strlen(client_input)), 0, p->ai_addr, p->ai_addrlen)) == -1) {
		perror("talker: sendto");
		exit(1);
	}

	if((numbytes = sendto(sockfd, link_info, (sizeof(char*) * strlen(link_info)), 0, p->ai_addr, p->ai_addrlen)) == -1) {
		perror("talker: sendto");
		exit(1);
	}

	freeaddrinfo(servinfo);

	char* linkID_str = strtok(client_input, ",");
	char* size_str = strtok(NULL, ",");
	char* power_str = strtok(NULL, ",");

	printf("The AWS sent link ID=<%s>, size=<%s>, power=<%s>, and link information to Backend-Server C using UDP over port <%s>.\n", linkID_str, size_str, power_str, UDP_PORT);

	recvfrom(sockfd,result, sizeof(char)*50, 0, p->ai_addr, &p->ai_addrlen);

	printf("The AWS received outputs from Backend-Server C using UDP over port <%s>.\n",UDP_PORT);
}

//Adapted Beej's Guide to Network Programming to establish UDP connections with serverA and serverB

/* int sendToDatabaseServers(char server, char* linkID, char* link_info)
* 1) Establishes UDP connection with specified server (serverA or serverB)
* 2) Sends linkId to serverA/B via UDP 
* 3) Receives a int (0 or 1) depending on whether a match is found via UDP
* 4) If recived a 1, receives the link_info (linkID, bandwidth, length, velocity, noise power) and stores in link_info
* 5) Returns 0 or 1 if match is found
*
*/
int sendToDatabaseServers(char server, char* linkID, char* link_info) {
	
	int sockfd;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	int rv;
	int numbytes;

	char* server_port;

	if(server == 'A') {
		server_port = PORT_A;
	} else if (server == 'B') {
		server_port = PORT_B;
	} else if (server == 'C') {
		server_port = PORT_C;
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;

	if((rv = getaddrinfo("localhost", server_port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("talker:socket");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "talker: failed to create socket\n");
		exit(1);
	}

	if((numbytes = sendto(sockfd, linkID, (sizeof(char*) * strlen(linkID)), 0, p->ai_addr, p->ai_addrlen)) == -1) {
		perror("talker: sendto");
		exit(1);
	}

	freeaddrinfo(servinfo);

	printf("The AWS sent link ID=<%s> to Backend-Server <%c> using UDP over port <%s>.\n", linkID, server, UDP_PORT);

	int result; 
	recvfrom(sockfd, &result, sizeof(result), 0, p->ai_addr, &p->ai_addrlen);

	printf("The AWS received <%d> matches from Backend-Server <%c> using UDP over port <%s>.\n", result, server, UDP_PORT);
	
	if(result == 1) {
		recvfrom(sockfd, link_info, sizeof(char)*50, 0, p->ai_addr, &p->ai_addrlen);

	}

	close(sockfd);
	return result;
}

//Beej's Guide to Network Programming for getSockFd

/* int getSockFd(char* port_no)
*  1) Binds sockfd to appropriate socket passed on the port number passed in
*  2) Returns sockfd
*/

int getSockFd(char* port_no) {
	int sockfd;

	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	struct sockaddr_storage their_addr;

	socklen_t sin_size;
	struct sigaction sa;

	int yes = 1;
	char s[INET6_ADDRSTRLEN];
	int rv;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;


	if((rv = getaddrinfo(NULL, port_no, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}


	if(p == NULL) {
		fprintf(stderr, "server: failed to bind\n");
		exit(1);
	}

	freeaddrinfo(servinfo);
   
	if(listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	return sockfd;
}

//Adapted Beej's Guide to Network Programming to establish TCP connections with monitor and client

/* 1) Establishes TCP connections with monitor and client
*  2) Receives client input from client
*  3) Sends link ID to database servers
*  4) If match found, sends all required information to serverC, if not, informs monitor and client
*  5) Recives result from serverC
*  6) Sends result to monitor and client
*
*/
int main(void) {
	int client_sockfd;
	int new_client_fd;

	int monitor_sockfd;
	int new_monitor_fd;


	struct sockaddr_storage their_addr;

	client_sockfd = getSockFd(TCP_CLIENT);
	monitor_sockfd = getSockFd(TCP_MONITOR);

	printf("The AWS is up and running.\n");

	while(1) {
		
		socklen_t sin_size = sizeof(their_addr);
		new_client_fd = accept(client_sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if(new_client_fd == -1) {
			perror("accept");
			continue;
		}


		sin_size = sizeof(their_addr);
		new_monitor_fd = accept(monitor_sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if(new_monitor_fd == -1) {
			perror("accept");
			close(new_client_fd);
			continue;
		}


		if(!fork()) {
			close(client_sockfd);
			close(monitor_sockfd);

			char client_input [1024];

			if(recv(new_client_fd,client_input, 1024, 0) == -1) {
				perror("recv");
				exit(1);
			}

			char* client_input_monitor = strdup(client_input);
			char* client_input_serverC = strdup(client_input);

			char* linkID_str = strtok(client_input, ",");
			char* size_str = strtok(NULL, ",");
			char* power_str = strtok(NULL, ",");

			printf("The AWS received link ID=<%s>, size=<%s>, and power=<%s> from the client using TCP over port <%s>.\n", linkID_str, size_str, power_str, TCP_CLIENT);

			if(send(new_monitor_fd, client_input_monitor, (sizeof(char*)*strlen(client_input_monitor)), 0) == -1) {
				perror("send");
				exit(1);
			}

			printf("The AWS sent link ID=<%s>, size=<%s>, and power=<%s> to the monitor using TCP over port <%s>.\n", linkID_str, size_str, power_str, TCP_MONITOR);


			char* link_info_A = malloc(sizeof(char*));
			int databaseAres = sendToDatabaseServers('A', linkID_str, link_info_A);
	
			char* link_info_B = malloc(sizeof(char*));
			int databaseBres= sendToDatabaseServers('B', linkID_str, link_info_B);

			if(databaseAres == 0 && databaseBres == 0) {
				char* no_result = malloc(sizeof(char*));
				no_result = "no matches";

				if(send(new_client_fd, no_result, (sizeof(char*) * strlen(no_result)), 0) == -1) {
				perror("send");
				exit(1);
				}

				if(send(new_monitor_fd, no_result, (sizeof(char*) * strlen(no_result)), 0) == -1) {
				perror("send");
				exit(1);
				}

				printf("The AWS sent No Match to the monitor and the client using TCP over ports <%s> and <%s>, respectively.\n", TCP_MONITOR, TCP_CLIENT);

			} else {
				char* result = malloc(sizeof(char*));

				if(databaseAres == 1) {
					sendToComputingServer(client_input_serverC, link_info_A, result);
				} else {
					sendToComputingServer(client_input_serverC, link_info_B, result);
				}

				char* result_monitor = strdup(result);
				char* trans_delay = strtok(result, ",");
				char* prop_delay = strtok(NULL, ",");
				char* delay = strtok(NULL, ",");
				

				if(send(new_client_fd, delay, (sizeof(char*)*strlen(delay)), 0) == -1) {
					perror("send");
					exit(1);
				}

				printf("The AWS sent delay=<%s>ms to the client using TCP over port <%s>.\n", delay, TCP_CLIENT);


				if(send(new_monitor_fd, result_monitor, (sizeof(char*)*strlen(result)), 0) == -1) {
					perror("send");
					exit(1);
				}

				printf("The AWS sent detailed results to the monitor using TCP over port <%s>.\n", TCP_MONITOR);
			}


			close(new_client_fd);
			close(new_monitor_fd);
			exit(0);
		}

		close(new_client_fd);
		close(new_monitor_fd);
	}


	return 0; 
}