#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#include <search.h>

#define PORT_B "22393"
#define MAXBUFLEN 100
#define DATABASE "database_b.csv"

extern char* searchDatabase(char* database, char* linkID);

//Beej's Guide to Network Programming for get_in_addr
void *get_in_addr(struct sockaddr *sa) {
	if(sa->sa_family = AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

//Adapted Beej's Guide to Network Programming to establish UDP Connection
/*
* 1) Establishes UDP Connection with AWS
* 2) Receives linkID from AWS
* 3) Searches appropriate database for a match
* 4) Sends <0> if no match found and <1> if match found to AWS via UDP
* 5) Sends link_info if matach was foudn to AWS via UDP
*/


int main(void) {
	
	int sockfd;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	int rv;
	int numbytes;
	struct sockaddr_storage their_addr;
	char buf[MAXBUFLEN];
	socklen_t addr_len;
	char s[INET6_ADDRSTRLEN];

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	if((rv = getaddrinfo(NULL, PORT_B, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}
		if(bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	freeaddrinfo(servinfo);

	printf("The Server B is up and running using UDP on port <%s>.\n", PORT_B);

	while(1) {
		addr_len = sizeof(their_addr);
		if((numbytes = recvfrom(sockfd, buf, MAXBUFLEN-1, 0, (struct sockaddr*)&their_addr, &addr_len)) == -1) {
			perror("recvfrom");
			exit(1);
		}

		

		inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*)&their_addr), s, sizeof(s));
		buf[numbytes] = '\0';
		char* linkID_str = strdup(buf);

		printf("The Server B received input <%s>.\n", linkID_str);

		char* search_result = malloc(sizeof(char*));
		search_result = searchDatabase(DATABASE, linkID_str);
		
		int result = 0;
		int* res = &result;

		if(search_result != NULL) {
			*res = 1;
		}

		printf("The server B has found <%d> match.\n", *res);

		if(sendto(sockfd, (char*)res, sizeof(res), 0, (struct sockaddr*)&their_addr, addr_len) == -1) {
			perror("sendto");
			exit(1);
		}

		if(*res == 1) {
			if(sendto(sockfd, (char*)search_result, sizeof(char)*(strlen(search_result)+1), 0, (struct sockaddr*)&their_addr, addr_len) == -1) {
				perror("sendto");
				exit(1);
			}
		}

		printf("The Server B finished sending the output to AWS\n");
	}
	
	return 0;

}