#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>

/* char* getLinkInfo(char* line, char* linkID)
* Determines if line passed starts with linkID,
* - If not, returns NULL
* - If yes, returns link info (linkID, bandwidth, length, velocity, noisepower) delimited by commas
*/
char* getLinkInfo(char* line, char* linkID) {
	char* token;
	char* line_copy = strdup(line);
	
	token = strtok(line, ",");

	if(strcmp(token, linkID) == 0) {
		return line_copy;

	}

	return NULL;
}

/* char* searchDatabase(char* database, char*linkID)
* 1) Opens file specified by database
* 2) Gets each line in file and checks if linkID is present
*  - Returns NULL, if no match is found
*  - Returns link_info, if match found
*/
char* searchDatabase(char* database, char* linkID) {
	
	FILE* stream = fopen(database, "r");
	char line[1024];
	char* res;

	while(fgets(line, 1024, stream)) {
		char* tmp = strdup(line);
		res = getLinkInfo(tmp, linkID);		
		if(res != NULL) {
			break;
		}
		
	}

	return res;
}