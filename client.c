#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#define AWS_PORT "25393"
#define HOST "localhost"
#define MAXDATASIZE 100

// Beej's Guide to Network Programming for get_in_addr
void *get_in_addr(struct sockaddr *sa) {
	if(sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

// Adapted Beej's Guides to Network Programming to establish TCP connection with AWS
/* 
*  1) Concatenates client input into single string
*  2) Sends string to AWS
*  3) Receives result (whether or not a match was found) and display appropriate messages
*/
int main(int argc, char* argv[]) {
	int sockfd;
	int numbytes;

	char buf[MAXDATASIZE];

	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;

	int rv;
	char s[INET6_ADDRSTRLEN];

	if(argc != 4) {
		fprintf(stderr, "usage: client hostname\n");
		exit(1);
	}


	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if((rv = getaddrinfo(HOST, AWS_PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	for(p = servinfo; p != NULL; p = p->ai_next) {
		if((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if(connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}

	if(p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	freeaddrinfo(servinfo);
	printf("The client is up and running.\n");

	char* client_input = strdup(argv[1]);
	strcat(client_input, ",");
	strcat(client_input, argv[2]);
	strcat(client_input, ",");
	strcat(client_input, argv[3]);

	if(send(sockfd, client_input, (sizeof(char*)*strlen(client_input)), 0) == -1) {
		perror("send");
		exit(1);
	}

	printf("The client sent ID=<%s>, size=<%s>, and power=<%s> to AWS.\n", argv[1], argv[2], argv[3]);

	char result[1024];

	if(recv(sockfd, result, 1024, 0) == -1) {
		perror("recv");
		exit(1);
	}

	if(strcmp(result, "no matches") == 0) {

		printf("Found no matches for link <%s>.\n", argv[1]);

	} else {

		printf("The delay for link<%s> is <%s>ms.\n", argv[1], result);
	}
	

	return 0;
}