#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <math.h>

/* double dBToWatts(double power)
* Converts dB to Watts and returns Watts
*/
double dBToWatts(double power) {
	return pow(10, power/10) / 1000;
}

/* char* compute(char* client_input, char* link_info)
* 1) Parses client input and link info and converts into ints and doubles 
* 2) Calculates transmission delay
* 3) Calculates propagation delay
* 4) Calculates end-to-end delay
* 5) Converts all the delays and concatenates into result string (transmission delay, propagation delay, end-to-end delay)
* 6) Returns result string
*/
char* compute(char* client_input, char* link_info) {

	//parse client input
	char* linkID_str = strtok(client_input, ",");
	char* size_str = strtok(NULL, ",");
	char* power_str = strtok(NULL, ",");

	double size = atof(size_str);
	double power = atof(power_str);

	//parse link info
	double bw;
	double length;
	double velocity;
	double noise_power;

	char* token;
	token = strtok(link_info, ",");
	token = strtok(NULL, ",");
	bw = atof(token);

	token = strtok(NULL, ",");
	length = atof(token);

	token = strtok(NULL, ",");
	velocity = atof(token);

	token = strtok(NULL, ",");
	noise_power = atof(token);

	//calculate bandwidth
	double bandwidth = (bw *1000000) * log2(1 + (dBToWatts(power)/ dBToWatts(noise_power)));

	//calculate transmission delay
	double transmission_delay = (size) / (bandwidth);
	char trans_delay[20];
	sprintf(trans_delay, "%.2f", transmission_delay * 1000);

	//calculate propagation delay
	double propagation_delay = (length*1000) / (velocity * 10000000);
	char prop_delay[20];
	sprintf(prop_delay, "%.2f", propagation_delay*1000);

	//calculate end-to-end delay
	double end_to_end_delay = transmission_delay + propagation_delay;
	char end_delay[20];
	sprintf(end_delay, "%.2f", end_to_end_delay*1000);

	strcat(trans_delay, ",");
	strcat(trans_delay, prop_delay);
	strcat(trans_delay, ",");
	strcat(trans_delay, end_delay);

	char* result = malloc(sizeof(char*));
	result = strdup(trans_delay);

	return result;
}