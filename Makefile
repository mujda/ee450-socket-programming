aws: awsoutput
	./awsoutput

monitor: monitoroutput
	./monitoroutput

serverA: serverAoutput
	./serverAoutput

serverB: serverBoutput
	./serverBoutput

serverC: serverCoutput
	./serverCoutput

all: aws.c client.c monitor.c serverA.o serverB.o serverC.o search.o compute.o
	gcc -o awsoutput aws.c
	gcc -o client client.c
	gcc -o monitoroutput monitor.c
	gcc -o serverAoutput -g serverA.o search.o
	gcc -o serverBoutput -g serverB.o search.o
	gcc -o serverCoutput -g serverC.o compute.o -lm

serverA.o: serverA.c search.h
	gcc -g -c serverA.c

serverB.o: serverB.c search.h
	gcc -g -c serverB.c

serverC.o: serverC.c compute.h
	gcc -g -c serverC.c

search.o: search.c search.h
	gcc -c -c  search.c

compute.o: compute.c compute.h
	gcc -c -c compute.c -lm

clean:
	rm -f *.o all
	rm awsoutput client monitoroutput serverAoutput serverBoutput serverCoutput