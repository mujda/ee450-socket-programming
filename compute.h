#ifndef _COMPUTE_H_
#define _COMPUTE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <math.h>

/*
* Header file for compute.c
*/

extern char* compute(char* client_input, char* link_info);



#endif /*_COMPUTE_H_*/