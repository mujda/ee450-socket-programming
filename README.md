Name: Mujda Alamzai

Student ID: 2692-5883-93

Summary of Assignment: 

* Implemented a server (AWS) that can support both TCP and UDP connections.
* Implemented two backend servers (ServerA and ServerB) that when provided a link ID by AWS over a UDP connection can look up the associated link information from a .csv file. The a <1> and the link information is returned if a match is found, and a <0> is returned if no matches are found. The results are delivered back to AWS via UDP.
* Implemented a computing server (Server C) that when provided the client input information and the associated link information computes the transmission delay, propagation delay and end-to-end delay of the link. The required inputs are provided by AWS over a UDP connection and the resulting calculations are delivered to AWS via UDP.
* Implemented a client that establishes a TCP connection with AWS and delivers via TCP the link ID, size and power the user wishes to compute the delay for. It a match for the provided link ID exists in either of the databases, the client displays the calculated end-to-end delay which it receives from AWS via TCP. If no match is found, the client displays that no matches were found and is informed of that circumstance by AWS via TCP.
* Implemented a monitor that establishes a TCP connections with AWS and receives via TCP from AWSthe link ID, size and power that was provided by the client to AWS. If no matches are found in the databases AWS informs the monitor via TCP and the monitor displays that no matches were found. If a match is found, the monitor displays the transmission delay, propagation delay and end-to-end delay associated with the link.

Description of code files:

aws.c - Implements the AWS server functionality
client.c - Implements the client functionality
monitor.c - Implements the monitor functionality
serverA.c - Implements the database server functionality for database A
serverB.c - Implements the database server functionality for database B
serverC.c - Implements the computing server functionality
compute.c - Computes the end-to-end delay, transmission delay and propagation delay when provided all the required inputs
compute.h - Header file associated with compute.c
search.c - Searches the specificed database when provided a linkID
search.h - Header file associated with search.c

Format of all messages exchanged:

All messages exchanged as strings with commas as delimiters
Server C takes two strings each with commas as delimiters (see below)

Client -> AWS: linkID, size, power
AWS -> Monitor: linkID, size, power
AWS -> ServerA/ServerB: linkID
ServerA/B -> AWS: m
ServerA/B -> AWS: linkID, bandwidth, length, velocity, noise power
AWS-> ServerC: client_input(linkID, size, power), link_info(linkID, bandwidth, length, velocity, noise power)
ServerC -> AWS: transmission delay, propagation delay, end-to-end delay
AWS -> Monitor: "no matches" (if no matches found) OR transmission delay, propagation delay, end-to-end delay
AWS -> Client : "no matches" (if no matches found) OR transmission delay, propagation delay, end-to-end delay

Failure Conditions: None

Resued Code:

Beej's Guide to Network Programming Using Internet Sockets Version 3.0.21 (See source code in-line comments)

